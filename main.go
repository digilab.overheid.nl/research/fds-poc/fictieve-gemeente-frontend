package main

import (
	"errors"
	"fmt"
	"html/template"
	"log"
	"net/http"
	"net/url"

	"github.com/gofiber/fiber/v2"
	"github.com/gofiber/fiber/v2/middleware/logger"
	"github.com/gofiber/template/html/v2"
	"github.com/google/uuid"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-gemeente-frontend/backend"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-gemeente-frontend/config"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-gemeente-frontend/helpers"
	"gitlab.com/digilab.overheid.nl/research/fds-poc/fictieve-gemeente-frontend/model"
)

func main() {
	// Template engine and functions
	engine := html.New("./views", ".html")

	engine.AddFuncMap(template.FuncMap{
		"configMunicipality": func() interface{} { return config.Config.Municipality },
		"numberFormat":       helpers.NumberFormat,
		"formatDate":         helpers.FormatDate,
		"formatYear":         helpers.FormatYear,
	})

	// Fiber instance
	app := fiber.New(fiber.Config{
		Views:       engine,
		ViewsLayout: "layouts/main",

		// Override default error handler
		ErrorHandler: func(c *fiber.Ctx, err error) error {
			// Status code defaults to 500
			code := fiber.StatusInternalServerError

			// Retrieve the custom status code if it's a *fiber.Error
			var e *fiber.Error
			if errors.As(err, &e) {
				code = e.Code
			}

			// Send custom error page
			if err = c.Status(code).Render("error", fiber.Map{
				"title":   fmt.Sprintf("Fout %d", code),
				"code":    code,
				"details": err.Error(),
			}); err != nil {
				// In case serving the error page fails
				return c.Status(code).SendString(err.Error())
			}

			return nil
		},
	})

	// Middleware
	app.Use(logger.New(logger.Config{
		Format: "${time} | ${status} | ${latency} | ${method} | ${path}   ${error}\n", // Do not log IP addresses. Note: see https://docs.gofiber.io/api/middleware/logger#constants for more logger variables
	}))

	// Routes
	app.Static("/", "./public")

	// security.txt redirect
	app.Get("/.well-known/security.txt", func(c *fiber.Ctx) error {
		return c.Redirect("https://www.ncsc.nl/.well-known/security.txt", fiber.StatusFound) // StatusFound is HTTP code 302
	})

	// Healthz endpoint
	app.Get("/healthz", func(c *fiber.Ctx) error {
		return c.SendString(".") // Send a period as response body, similar to chi's Heartbeat middleware
	})

	// WOZ objects
	app.Get("/", func(c *fiber.Ctx) error {
		// Fetch the WOZ objects from the WOZ objects backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10")) // IMPROVE: use implicitly, do not include in query parameters?
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		var response model.Response[model.WOZObject]
		if err := backend.Request(http.MethodGet, config.Config.WOZObjecten.Domain, fmt.Sprintf("/woz-objects?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.WOZObjecten.FSCGrantHash}, &response); err != nil {
			return fmt.Errorf("error fetching WOZ objects: %w", err)
		}

		return c.Render("index", fiber.Map{
			"title":      "WOZ-objecten",
			"objects":    response.Data,
			"pagination": response.Pagination,
		})
	})

	app.Get("/woz-objects/:id", func(c *fiber.Ctx) error {
		// Fetch the WOZ object from the WOZ objects backend
		objectID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		object := new(model.WOZObject)
		if err := backend.Request(http.MethodGet, config.Config.WOZObjecten.Domain, fmt.Sprintf("/woz-objects/%s", objectID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.WOZObjecten.FSCGrantHash}, object); err != nil {
			return fmt.Errorf("WOZ object request failed: %w", err)
		}

		return c.Render("woz-object-details", fiber.Map{
			"title":  fmt.Sprintf("WOZ-object: %s", object.FormattedType()),
			"object": object,
		})
	})

	// People
	people := app.Group("/people")

	people.Get("/", func(c *fiber.Ctx) error {
		// Fetch the people from the Fictief Register Personen backend
		query := url.Values{}
		query.Set("perPage", c.Query("perPage", "10"))
		query.Set("lastId", c.Query("lastId"))
		query.Set("firstId", c.Query("firstId"))

		response := new(model.Response[model.Person])
		if err := backend.Request(http.MethodGet, config.Config.FRP.Domain, fmt.Sprintf("/people?%s", query.Encode()), nil, map[string]string{"Fsc-Grant-Hash": config.Config.FRP.FSCGrantHash}, response); err != nil {
			return fmt.Errorf("error fetching people: %w", err)
		}

		return c.Render("person-index", fiber.Map{
			"title":      "Personen",
			"people":     response.Data,
			"pagination": response.Pagination,
		})
	})

	people.Get("/:id", func(c *fiber.Ctx) error {
		// Fetch the person from the Fictief Register Personen backend
		buildingID, err := uuid.Parse(c.Params("id"))
		if err != nil {
			return fmt.Errorf("uuid parse failed: %w", err)
		}

		person := new(model.Person)
		if err := backend.Request(http.MethodGet, config.Config.FRP.Domain, fmt.Sprintf("/people/%s", buildingID), nil, map[string]string{"Fsc-Grant-Hash": config.Config.FRP.FSCGrantHash}, person); err != nil {
			return fmt.Errorf("person request failed: %w", err)
		}

		return c.Render("person-details", fiber.Map{
			"title":  fmt.Sprintf("Persoon: %s", person.Name),
			"person": person,
		})
	})

	// Start server
	if err := app.Listen(config.Config.ListenAddress); err != nil { // Note: port should never be nil, since flag.Parse() is run
		log.Println("error starting server:", err)
	}
}
