package model

import (
	"errors"
	"fmt"
	"time"

	"github.com/google/uuid"
)

type StakeholderType string

type WOZObjectType string

const (
	NaturalPerson    StakeholderType = "naturalPerson"
	NonNaturalPerson StakeholderType = "nonNaturalPerson"

	Residential    WOZObjectType = "residential"
	NonResidential WOZObjectType = "non-residential"
)

var (
	ErrUnknownObjectType = errors.New("unknown object type")
)

type WOZObject struct {
	ID                  uuid.UUID        `json:"id"`
	AddressableObjectID uuid.UUID        `json:"addressableObjectId"`
	StakeholderOwner    *Stakeholder     `json:"stakeholderOwner,omitempty"`
	StakeholderOccupant *Stakeholder     `json:"stakeholderOccupant,omitempty"`
	Values              []WOZObjectValue `json:"values,omitempty"`
	RegisteredPeople    int              `json:"registeredPeople"`
	Type                WOZObjectType    `json:"type"`
	CreatedAt           time.Time        `json:"createdAt"`
}

type Stakeholder struct {
	ID   uuid.UUID       `json:"id"`
	BSN  string          `json:"burgerServiceNummer,omitempty"`
	RSIN string          `json:"rsin,omitempty"`
	Type StakeholderType `json:"type"`
}

type WOZObjectValue struct {
	ID          uuid.UUID  `json:"id"`
	WOZObjectID uuid.UUID  `json:"wozObjectId"`
	Value       int        `json:"value"`
	ValuationAt time.Time  `json:"valuationAt"`
	EffectiveAt *time.Time `json:"effectiveAt,omitempty"`
}

func (obj *WOZObject) LastEffectiveValue() (value int) {
	var lastEffective *time.Time

	for _, val := range obj.Values {
		// If same or later date than already found: use this value
		if lastEffective == nil || (val.EffectiveAt != nil && !val.EffectiveAt.Before(*lastEffective)) {
			value = val.Value
			lastEffective = val.EffectiveAt
		}
	}

	return
}

func (obj *WOZObject) FormattedType() string {
	switch obj.Type {
	case Residential:
		return "Woning"

	case NonResidential:
		return "Niet-woning"
	}

	return "(Onbekend)"
}

func (obj *Stakeholder) FormattedIdentifier() string {
	switch obj.Type {
	case NaturalPerson:
		return fmt.Sprintf("BSN: %s", obj.BSN)

	case NonNaturalPerson:
		return fmt.Sprintf("RSIN: %s", obj.RSIN)
	}

	return "(Onbekend)"
}
